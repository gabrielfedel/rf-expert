from org.csstudio.display.builder.runtime.script import PVUtil


ch = PVUtil.getString(pvs[0])
prefix = PVUtil.getString(pvs[1])
prefixdig = PVUtil.getString(pvs[2])
pitype = PVUtil.getString(pvs[3])

# set link between raw value and new point
pv_new_point = PVUtil.createPV(prefix + pitype +   "CalStatsNewVal.INP", 5000)
pv_new_point.setValue(prefixdig + ch + "-SMonAvg-Mag CP")

PVUtil.releasePV(pv_new_point)
